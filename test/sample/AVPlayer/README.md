#  AVPlayer

### Introduction

This sample shows the basic functions of the **AVPlayer** component，how to control the related capabilities of the playback state. Including audio playback, video playback, network playback, etc.

1.Enter the homepage and click the mode to be played.

2.Click on the preset **video** or **audio** you want to play.

3.Enter the play interface and click the play button to start playing.

4.Click the buttons such as **pause**, switch video, **loop** playback, and double **speed** to control the playback operation

### Display Effect

<img src="screenshots\device\home.png" alt="home" style="zoom:12%;" /> <img src="screenshots\device\list.png" alt="list" style="zoom:12%;" /> <img src="screenshots\device\play.png" alt="play" style="zoom:12%;" /> 

### Required Permissions

This sample requires configuring the following permissions in module. json5:

Write media file permissions：ohos.permission.WRITE_MEDIA

Read media file permissions： ohos.permission.READ_MEDIA

### Constraints

1.This sample only supports running on a standard system. Supported devices: NOAH/WGR/RK3568.

2.This sample only supports the API version 9 SDK.

3.This sample requires DevEco Studio 3.1 (Build Version: 3.0.1.200, build on Library 16, 2023) to be compiled and run.



